﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JJMConsultants.Models
{
    public class user
    {
        public string name { get; set; }
        public string email { get; set; }   
        public string phone { get; set; }
        public string degree { get; set; }
        public string address { get; set; }
        public string about { get; set; }
        public string message { get; set; }
    }
}