﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Script.Services;
using System.Web.Services;
using JJMConsultants.Models;

namespace JJMConsultants.Controllers
{
    public class IndexController : ApiController
    {

        //[HttpPost]
        [WebMethod]
        [ScriptMethod]
        public HttpResponseMessage ModalEmail(user user)
        {
            try
            {
                string name = user.name ?? "";
                string phone = user.phone ?? "";
                string email = user.email ?? "";
                string degree = user.degree ?? "";
                string address = user.address ?? "";
                string about = user.about ?? "";

                string msgTo = ConfigurationManager.AppSettings["msgTo"];
                string username = ConfigurationManager.AppSettings["email"];
                string password = ConfigurationManager.AppSettings["password"];

                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(email);
                // Recipient e-mail address.
                Msg.To.Add(msgTo);
                Msg.ReplyToList.Add(new MailAddress(email));
                Msg.Subject = "Job hiring request from : " + name;

                Msg.Body = "Name : " + name + "\nEmail id : " + email + "\nPhone # : " + phone + "\n Address : " + address + "\n Education qualification : " + degree + "\n About : " + about;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                //smtp.Credentials = new System.Net.NetworkCredential("rides.valleytransport@gmail.com", "valley@1");
                smtp.Credentials = new System.Net.NetworkCredential(username, password);
                smtp.EnableSsl = true;
                smtp.Send(Msg);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = true;
                Msg = null;



                return Request.CreateResponse(HttpStatusCode.OK, "Mail successfully sent.");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Server error occured"+ex.ToString());

            }
        }

        //[HttpPost]
        [WebMethod]
        [ScriptMethod]
        public HttpResponseMessage ContactUs(user user)
        {
            try
            {
                string name = user.name ?? "";
                string email = user.email ?? "";
                string message = user.message ?? "";

                string msgTo = ConfigurationManager.AppSettings["msgTo"];
                string username = ConfigurationManager.AppSettings["email"];
                string password = ConfigurationManager.AppSettings["password"];

                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                if (!string.IsNullOrWhiteSpace(email))
                    Msg.From = new MailAddress(email);
                // Recipient e-mail address.
                Msg.To.Add(msgTo);
                if (!string.IsNullOrWhiteSpace(email))
                    Msg.ReplyToList.Add(new MailAddress(email));
                Msg.Subject = "Job hiring request from : " + name;

                Msg.Body = "Name : " + name + "\nEmail id : " + email + "\n Message : " + message;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                //smtp.Credentials = new System.Net.NetworkCredential("rides.valleytransport@gmail.com", "valley@1");
                smtp.Credentials = new System.Net.NetworkCredential(username, password);
                smtp.EnableSsl = true;
                smtp.Send(Msg);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = true;
                Msg = null;



                return Request.CreateResponse(HttpStatusCode.OK, "Mail successfully sent.");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Server error occured"+ex.ToString());

            }
        }

        [HttpGet]
        public string Get()
        {
            return "Hello";
        }
    }
}
